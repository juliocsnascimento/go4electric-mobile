import 'dart:convert';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:go4electric/model/User.dart';
import 'package:go4electric/service/auth.service.dart';
import 'package:http/http.dart' as http;

class RestClient {
  // static const _DEBUG = false;
  static final _baseApi =
      'https://us-central1-go4electric-dev.cloudfunctions.net/';
  static const httpTimeout = Duration(seconds: 60);
  static const genericTimeout = Duration(seconds: 5);
  static RestClient _instance;

  RestClient._internal();

  factory RestClient() {
    if (_instance == null) {
      _instance = RestClient._internal();
    }
    return _instance;
  }

  Future<User> getUser() {
    final auth = AuthService();
    return auth.go4user.timeout(genericTimeout);
  }

  Future<http.Response> get(String api, {String token}) async {
    http.Response response = await http.get(_baseApi + api, headers: {
      'Content-type': 'application/json',
      'x-access-token': token
    }).timeout(httpTimeout);

    if (response.statusCode == 401) {
      print("Token Expired. Getting new....");
      User _user = await this.getUser();
      _user.token = (await _user.firebaseUser.getIdToken(refresh: true));
      response = await http.get(_baseApi + api, headers: {
        'Content-type': 'application/json',
        'x-access-token': _user.token.token
      }).timeout(httpTimeout);
    }
    if (response != null && response.statusCode == 200) {
      return response;
    } else {
      throw (response.statusCode);
    }
  }

  Future<http.Response> post(String api, dynamic body, {String token}) async {
    print(_baseApi + api);
    http.Response response = await http
        .post(_baseApi + api,
            headers: {
              'Content-type': 'application/json',
              'x-access-token': token
            },
            body: json.encode(body))
        .timeout(httpTimeout);

    if (response.statusCode == 401) {
      print("Token Expired. Getting new....");
      User _user = await this.getUser();
      _user.token = (await _user.firebaseUser.getIdToken(refresh: true));

      response = await http
        .post(_baseApi + api,
            headers: {
              'Content-type': 'application/json',
              'x-access-token': _user.token.token
            },
            body: json.encode(body))
        .timeout(httpTimeout);
    }

    if (response != null && response.statusCode == 200) {
      return response;
    } else {
      throw (response.statusCode);
    }
  }

  Future<http.Response> del(String api, {String token}) async {
    http.Response response = await http.delete(_baseApi + api, headers: {
      'Content-type': 'application/json',
      'x-access-token': token
    }).timeout(httpTimeout);

    if (response.statusCode == 401) {
      print("Token Expired. Getting new....");
      User _user = await this.getUser();
      _user.token = (await _user.firebaseUser.getIdToken(refresh: true));
      response = await http.delete(_baseApi + api, headers: {
        'Content-type': 'application/json',
        'x-access-token': _user.token.token
      }).timeout(httpTimeout);
    }
    if (response != null && response.statusCode == 200) {
      return response;
    } else {
      throw (response.statusCode);
    }
  }
}
