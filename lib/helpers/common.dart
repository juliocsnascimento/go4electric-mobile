import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BasicLayout extends StatelessWidget {
  BasicLayout({this.child, this.bottomnavigationbar});

  final Widget child;
  final Widget bottomnavigationbar;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: true,
      body: Container(
        constraints: BoxConstraints.expand(),
        child: SafeArea(
          child: child,
        ),
      ),
      bottomNavigationBar: bottomnavigationbar,
    );
  }
}

class PrimaryButton extends StatelessWidget {
  PrimaryButton({this.buttonText, this.width, this.onPressed});

  final String buttonText;
  final double width;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8.0),
          shape: BoxShape.rectangle,
          color: Theme.of(context).primaryColor),
      child: FlatButton(
        child: Text(
          buttonText.toUpperCase(),
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.white,
            fontSize: 17.0,
            fontWeight: FontWeight.bold,
          ),
        ),
        onPressed: onPressed,
      ),
    );
  }
}

class SecondaryButton extends StatelessWidget {
  SecondaryButton({this.buttonText, this.width, this.onPressed});

  final String buttonText;
  final double width;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8.0),
          shape: BoxShape.rectangle,
          color: Colors.black26),
      child: FlatButton(
        child: Text(
          buttonText,
          textAlign: TextAlign.center,
          style: TextStyle(
              color: Colors.white, fontSize: 17.0, fontWeight: FontWeight.bold),
        ),
        onPressed: onPressed,
      ),
    );
  }
}

abstract class Go4Dialog {
  static Future<bool> questionAlert(String alertText, String acceptText,
      String cancelText, BuildContext context,
      {dynamic onPressed}) {
    final double padSize = (alertText.length < 80) ? 40.0 : 2.0;
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
              contentPadding: EdgeInsets.only(
                  left: 5.0, right: 5.0, top: 15.0, bottom: 5.0),
              content: Container(
                  width: double.infinity,
                  height: 200.0,
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: padSize),
                          child: Text(alertText, textAlign: TextAlign.center),
                        ),
                        Row(
                          children: <Widget>[
                            Expanded(
                                child: Padding(
                              child: FlatButton(
                                color: Theme.of(context).primaryColor,
                                child: Text(
                                  acceptText,
                                  style: TextStyle(
                                      fontSize: 20.0, color: Colors.white),
                                ),
                                onPressed: onPressed ??
                                    () {
                                      Navigator.of(context).pop(true);
                                    },
                              ),
                              padding: const EdgeInsets.only(left: 8, right: 8),
                            )),
                            Expanded(
                                child: Padding(
                              child: FlatButton(
                                color: Theme.of(context).primaryColor,
                                child: Text(
                                  cancelText,
                                  style: TextStyle(
                                      fontSize: 20.0, color: Colors.white),
                                ),
                                onPressed: () {
                                  Navigator.of(context).pop(true);
                                },
                              ),
                              padding: const EdgeInsets.only(left: 8, right: 8),
                            )),
                          ],
                        )
                      ])));
        });
  }

  static Future<bool> showAlert(String alertText, BuildContext context) {
    final double padSize = (alertText.length < 80) ? 40.0 : 2.0;
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            contentPadding:
                EdgeInsets.only(left: 5.0, right: 5.0, top: 15.0, bottom: 5.0),
            content: Container(
              width: double.infinity,
              height: 200.0,
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: padSize),
                      child: Text(alertText, textAlign: TextAlign.center),
                    ),
                    FlatButton(
                      color: Theme.of(context).primaryColor,
                      child: Text(
                        "OK",
                        style: TextStyle(fontSize: 20.0, color: Colors.white),
                      ),
                      onPressed: () {
                        Navigator.of(context).pop(true);
                      },
                    )
                  ]),
            ),
          );
        });
  }
}
