enum ErrorCode {
  unknown,
  firebase_account_exists,
  rest,
  wrong_code,
  concurrency
}

class GoFourException implements Exception {

  final ErrorCode code;
  final String message;
  final Exception e;

  GoFourException({this.code, this.message, this.e});

  @override
  String toString() => 'Go4Exception: $code | $message | $e';
}

class CraftRestException extends GoFourException {
  final int httpStatusCode;
  GoFourException e;

  CraftRestException({this.httpStatusCode, String message}) {
    this.e = GoFourException(code: ErrorCode.rest, message: message, e: null);
  }

  @override
    String toString() {
      final s = super.toString();
      return 'httpStatus = $httpStatusCode | $s';
    }
}