import 'dart:convert';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:go4electric/helpers/restClient.dart';

class User {
  String uid;
  String fcmToken;
  String email;
  String name;
  String phone;
  String displayName;
  String cpf;
  String photo;
  String keyId;
  DateTime birthDay;
  FirebaseUser firebaseUser;
  IdTokenResult token;
  final RestClient rest;

  // final FirebaseAuth _auth = FirebaseAuth.instance;

  User({this.firebaseUser, this.token}) : rest = RestClient();

  factory User.fromFirebaseUser(
      FirebaseUser firebaseUser, IdTokenResult token) {
    User user = User(firebaseUser: firebaseUser, token: token);
    return user;
  }

  Future<void> save(
      {String email, String cpf, String birthDay, String photo}) async {
    final params = {
      "user": {
        "uid": this.uid,
        "name": name ?? this.name,
        "displayName": this.displayName,
        "email": email ?? this.email,
        "phone": this.phone,
        "birthday": birthDay ?? this.birthDay != null
            ? this.birthDay.toIso8601String()
            : null,
        "cpf": cpf ?? this.cpf,
        "photo": photo ?? this.photo,
        "fcmToken": [this.token.token]
      }
    };

    final response =
        await rest.post('users/currentUser/save', params, token: token.token);

    if (response.statusCode != 200) {
      print('Something went wrong ${response.statusCode}');
    }

    // print('response: ${response.body}');
  }

  Future<void> getUserData() async {
    final response = await rest.get('users/currentUser/', token: token.token);
    final userData = jsonDecode(response.body);
    if (userData['data'] != "NOT_FOUND") {
      this.email = userData['data']['email'];
      this.name = userData['data']['name'];
      this.cpf = userData['data']['cpf'];
      this.birthDay = DateTime.fromMillisecondsSinceEpoch(
          userData['data']['birthday']['_seconds'] * 1000);
      this.photo = userData['data']['photo'];
    }
  }

  @override
  String toString() {
    return "User: $displayName | $email | $name | $firebaseUser | $cpf | ${keyId?.length}";
  }
}
