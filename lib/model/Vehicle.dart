import 'package:go4electric/helpers/enumFromString.dart';
import 'package:go4electric/model/ConnectorType.enum.dart';
import 'package:go4electric/model/VehicleType.enum.dart';

class Vehicle {
  final String uid, brand, name, model;
  final ConnectorType connectorType;
  final VehicleType type;

  Vehicle(
      {this.uid,
      this.brand,
      this.name,
      this.model,
      this.connectorType,
      this.type});

  factory Vehicle.fromJson(Map<String, dynamic> json) {
    return Vehicle(
        uid: json['uid'].toString(),
        brand: json['brand'].toString(),
        name: json['name'].toString(),
        model: json['model'].toString(),
        connectorType: enumFromString<ConnectorType>(
            json['connectorType'], ConnectorType.values),
        type: enumFromString<VehicleType>(json['type'], VehicleType.values));
  }

  @override
  String toString() {
    String t = enumToString(type);
    return "$brand | $model | $t";
  }
}
