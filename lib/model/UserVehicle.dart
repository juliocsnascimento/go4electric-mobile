
import 'package:go4electric/helpers/enumFromString.dart';
import 'package:go4electric/model/State.enum.dart';
import 'package:go4electric/model/Vehicle.dart';

class UserVehicle {
  String uid;
  double lastBatteryStatus;
  String plate;
  bool recharging;
  State state;
  Vehicle type;

  UserVehicle(
      {this.uid,
      this.lastBatteryStatus,
      this.plate,
      this.recharging,
      this.state,
      this.type});

  factory UserVehicle.fromJson(Map<String, dynamic> json) {
    return UserVehicle(
      uid: json['id'].toString(),
      lastBatteryStatus: double.parse(json['lastBatteryStatus'].toString()),
      plate: json['plate'].toString(),
      recharging: json['recharging'],
      state: enumFromString<State>(json['state'], State.values),
      type: Vehicle.fromJson(json['type'])
    );
  }

  String get name => this.name;

  @override
  String toString() {
    return "Vehicle: $type | $plate ";
  }
}
