class ResponseData {
  bool status;
  dynamic data;

  ResponseData({this.status, this.data});
  
  factory ResponseData.fromJson(Map<String, dynamic> parsedJson) {
    return ResponseData(status: parsedJson['sttus'], data: parsedJson['data']);
  }
}
