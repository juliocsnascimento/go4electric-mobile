import 'dart:async';
import 'dart:convert';

import 'package:go4electric/helpers/restClient.dart';
import 'package:go4electric/model/User.dart';
import 'package:go4electric/model/UserVehicle.dart';
import 'package:go4electric/service/auth.service.dart';

class UserService {
  static final UserService _instance = new UserService._internal();

  factory UserService() {
    return _instance;
  }
  UserService._internal();

  final AuthService _auth = AuthService();
  User currentUser;

  Future<List<UserVehicle>> getUserVehicles() async {
    currentUser = await _auth.go4user;
    RestClient rest = RestClient();

    final response = await rest.get('/users/currentUser/vehicles',
        token: currentUser.token.token);
    if (response.statusCode == 200) {
      final parsed = json.decode(response.body);
      return (parsed["data"] as List)
          .map((e) => UserVehicle.fromJson(e))
          .toList();
    } else {
      print('error in get user vehicles request: ${response.statusCode}');
      return null;
    }
  }

  Future<bool> delUserVehicle(String vehicleId) async {
    currentUser = await _auth.go4user;
    RestClient rest = RestClient();
    print('users/currentUser/vehicles/' + vehicleId);

    final response = await rest.del('users/currentUser/vehicles/' + vehicleId,
        token: currentUser.token.token);

    print('pra felicidade do renato');
    print(vehicleId);
    if (response.statusCode != 200) {
      print('Something went wrong ${response.statusCode}');
      print(response.toString());
      return false;
    } else {
      print(response.statusCode);
      return true;
    }
  }

  Future<bool> saveUserVehicle(String plate, String typeId) async {
    currentUser = await _auth.go4user;
    RestClient rest = RestClient();

    final params = {
      "vehicle": {
        "lastBatteryStatus": 0,
        "plate": plate,
        "recharging": false,
        "active": 'active',
        "type": typeId
      }
    };

    final response = await rest.post('users/currentUser/vehicles/add', params,
        token: currentUser.token.token);

    if (response.statusCode != 200) {
      print('Something went wrong ${response.statusCode}');
      print(response.toString());
      return false;
    } else {
      return true;
    }
  }
}
