import 'dart:convert';

import 'package:go4electric/helpers/restClient.dart';
import 'package:go4electric/model/User.dart';
import 'package:go4electric/service/auth.service.dart';

class AppConfig {
  static final AppConfig _appConfig = AppConfig._internal();

  factory AppConfig() {
    return _appConfig;
  }

  AppConfig._internal();

  final AuthService _auth = AuthService();
  User currentUser;

  Future<String> getPrivacyPolicy() async {
    currentUser = await _auth.go4user;
    RestClient rest = RestClient();

    final response =
        await rest.get('/config/privacyPolicy', token: currentUser.token.token);

    if (response.statusCode == 200) {
      final parsed = json.decode(response.body);
      return parsed["data"]["text"];
    } else {
      print('error in privacy policy request: ${response.statusCode}');
      return null;
    }
  }


  Future<String> getTermsOfUse() async {
    currentUser = await _auth.go4user;
    RestClient rest = RestClient();

    final response =
        await rest.get('/config/termsOfUse', token: currentUser.token.token);

    if (response.statusCode == 200) {
      final parsed = json.decode(response.body);
      return parsed["data"]["text"];
    } else {
      print('error in termsOfUse request: ${response.statusCode}');
      return null;
    }
  }
}
