import 'dart:convert';

import 'package:go4electric/helpers/restClient.dart';
import 'package:go4electric/model/User.dart';
import 'package:go4electric/model/Vehicle.dart';
import 'package:go4electric/service/auth.service.dart';

class VehicleService {
  static final VehicleService _instance = new VehicleService._internal();

  factory VehicleService() {
    return _instance;
  }
  VehicleService._internal();

  final AuthService _auth = AuthService();
  User currentUser;

  Future<List<Vehicle>> getVehicles() async {
    currentUser = await _auth.go4user;
    RestClient rest = RestClient();

    final response = await rest.get('/vehicles',
        token: currentUser.token.token);
    if (response.statusCode == 200) {
      final parsed = json.decode(response.body);
      return (parsed["data"] as List)
          .map((e) => Vehicle.fromJson(e))
          .toList();
    } else {
      print('error in get vehicles request: ${response.statusCode}');
      return null;
    }
  }
}
