import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:go4electric/helpers/exceptions.dart';
import 'package:go4electric/model/User.dart';
import 'package:go4electric/pages/TabContent.dart';
import 'package:rxdart/subjects.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthService {
  static const genericTimeout = Duration(seconds: 8);
  String verificationId = "";
  bool errorOcurred = false;
  String temporaryPhoneNumberHolder;
  String userName = "";
  FirebaseAuth _auth;

  BehaviorSubject<User> userBehaviorSubject;
  Future<User> get go4user => userBehaviorSubject.first;

  Future<SharedPreferences> _sprefs = SharedPreferences.getInstance();

  // Let´s make this class a sngleton one!
  static final AuthService _instance = AuthService._internal();
  factory AuthService() {
    return _instance;
  }
  AuthService._internal() : _auth = FirebaseAuth.instance {
    _auth.setLanguageCode("pt-BR"); // set SMS locale
    userBehaviorSubject = BehaviorSubject<User>();
    _auth.onAuthStateChanged.listen((firebaseUser) {
      _updateUserData(firebaseUser);
    }, onError: (error) {
      print(error);
      _auth.signOut();
    }, cancelOnError: false).onDone(() => userBehaviorSubject.close());
  }

  /// Initialize user with (a few) data from Firebase user (not the Firestore database)
  _updateUserData(final FirebaseUser firebaseUser) async {
    if (firebaseUser != null) {
      try {
        IdTokenResult token = await firebaseUser.getIdToken();
        final SharedPreferences prefs = await _sprefs;
        final go4user = User.fromFirebaseUser(firebaseUser, token);
        go4user.getUserData().then((x) {
          go4user.displayName = go4user.displayName == null
              ? prefs.getString('nameLogin')
              : go4user.displayName;
          go4user.phone = prefs.getString('cellLogin');
          go4user.save();
          userBehaviorSubject.add(go4user);
        });
      } catch (error) {
        userBehaviorSubject.addError(error);
        _auth.signOut();
      }
    } else {
      print('Firebase user is null.');
    }
  }

  /// Verify link account exceptions thrown by platform (ios vs android)
  // bool _isFirebaseLinkAccountException(err, {stacktrace}) {
  //   if ((err.code == 'exception' &&
  //           (err.message as String)
  //               .contains('already')) // account already exists (ios)
  //       ||
  //       (err.code == 'sign_in_failed' && err.message == 'FIRAuthErrorDomain') &&
  //           err.details.contains('already')) // account already exists (android)
  //   {
  //     return true;
  //   } else {
  //     print('unexpected link error: $err');
  //     if (stacktrace != null) {
  //       print(stacktrace);
  //     }
  //     return false;
  //   }
  // }

  Future<dynamic> isLogged() async {
    return (_auth.currentUser() != null);
  }

  Future<void> signOut() async {
    await _auth.signOut();
  }

  Future<void> signUpByPhone(
      String phoneNumber, String displayName, BuildContext context) async {
    final PhoneVerificationCompleted verificationCompleted =
        (AuthCredential credential) async {
      print('verificationCompleted: ');
      final ok = await checkPhoneWithCredential(credential);

      if (ok) {
        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (BuildContext context) => TabContent()),
          ModalRoute.withName('/'),
        );
      }
    };

    final PhoneVerificationFailed verificationFailed =
        (AuthException authException) {
      print("Failed: " + authException.message);
      errorOcurred = true;
      throw GoFourException(
          code: ErrorCode.unknown,
          message: 'Unkown error occured',
          e: authException);
    };

    final PhoneCodeSent codeSent =
        (String verificationId, [int forceResendingToken]) async {
      print("codeSent VerificationID: " + verificationId);
      this.verificationId = verificationId;
      errorOcurred = false;
    };

    final PhoneCodeAutoRetrievalTimeout codeAutoRetrievalTimeout =
        (String verificationId) {
      print('-- codeAutoRetrievalTimeout --');
      errorOcurred = false;
    };

    temporaryPhoneNumberHolder = phoneNumber;
    try {
      await _auth.verifyPhoneNumber(
          phoneNumber: phoneNumber,
          timeout: genericTimeout,
          verificationCompleted: verificationCompleted,
          verificationFailed: verificationFailed,
          codeSent: codeSent,
          codeAutoRetrievalTimeout: codeAutoRetrievalTimeout);
    } catch (e) {
      throw GoFourException(
          code: ErrorCode.firebase_account_exists,
          message: 'Exception to register phone',
          e: e);
    }
  }

  Future<bool> checkPhoneCode(String codigo) async {
    final AuthCredential credential = PhoneAuthProvider.getCredential(
      verificationId: verificationId,
      smsCode: codigo,
    );
    AuthResult authResult;
    try {
      authResult = await _auth.signInWithCredential(credential);
    } catch (authException) {
      throw GoFourException(
          code: ErrorCode.wrong_code,
          message: 'Código errado foi inserido',
          e: authException);
    }

    FirebaseUser user = authResult.user;

    if (user != null) {
      user = await _auth.currentUser();
      _updateUserData(user);
    }
    final FirebaseUser currentUser = await _auth.currentUser();
    assert(user.uid == currentUser.uid);
    return (user.uid == currentUser.uid);
  }

  Future<bool> checkPhoneWithCredential(AuthCredential credential) async {
    print('strting checkPhoneWithCredential');

    AuthResult authResult = await _auth.signInWithCredential(credential);
    FirebaseUser user = authResult.user;

    if (user != null) {
      user = await _auth.currentUser();
      _updateUserData(user);
    }
    final FirebaseUser currentUser = await _auth.currentUser();
    assert(user.uid == currentUser.uid);
    return (user.uid == currentUser.uid);
  }
}
