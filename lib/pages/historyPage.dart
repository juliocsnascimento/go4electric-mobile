import 'package:flutter/material.dart';

class HistoryPage extends StatefulWidget {
  final String title;

  // constructor
  HistoryPage({Key key, this.title}) : super(key: key);

  @override
  _HistoryPageState createState() => _HistoryPageState();
}

class _HistoryPageState extends State<HistoryPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Histórico'),
        ),
        body: SingleChildScrollView());
  }
}
