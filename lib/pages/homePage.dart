import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:go4electric/pages/vehicles.dart';

class HomePage extends StatefulWidget {
  final String title;

  // constructor
  HomePage({Key key, this.title}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Widget _topBar() {
    return Container(
        height: 100,
        decoration: BoxDecoration(
          color: Theme.of(context).primaryColor,
        ),
        child: Padding(
          padding: const EdgeInsets.only(top: 20),
          child: ListTile(
            title: Text(
              "R\$ 100,00",
              textScaleFactor: 1.2,
              style: TextStyle(fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold),
            ),
            subtitle: Text('vencimento em 25/01/2020',
                style: TextStyle(color: Colors.white)),
            trailing: Icon(
              Icons.arrow_forward_ios,
              color: Colors.white,
            ),
            // onTap: () => Navigator.pushNamed(context, "/saldo"),
          ),
        ));
  }

  Widget _carsCard() {
    return Padding(
      padding: const EdgeInsets.all(5),
      child: Card(
          elevation: 2,
          child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
            Padding(
                padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 5),
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => VehiclesPage()));
                  },
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        Icons.directions_car,
                        size: 90,
                        color: Colors.white,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 15),
                        child: Text(
                          'Meus Veículos',
                          style: TextStyle(fontSize: 20, color: Colors.white),
                        ),
                      )
                    ],
                  ),
                ))
          ])),
    );
  }

  Widget _content() {
    return Container(
        width: double.infinity,
        height: double.infinity,
        decoration: BoxDecoration(
          color: Theme.of(context).canvasColor,
        ),
        child: GridView.count(
          crossAxisCount: 2,
          children: <Widget>[_carsCard()],
        ));
  }

  Widget _main() {
    return Column(
      children: <Widget>[
        _topBar(),
        Expanded(
          child: _content(),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: SafeArea(
        child: _main(),
      ),
    );
  }
}
