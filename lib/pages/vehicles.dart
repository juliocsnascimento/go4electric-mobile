import 'package:flutter/material.dart';
import 'package:go4electric/helpers/common.dart';
import 'package:go4electric/model/UserVehicle.dart';
import 'package:go4electric/model/VehicleType.enum.dart';
import 'package:go4electric/pages/newVehicle.dart';

import 'package:go4electric/service/user.service.dart';

class VehiclesPage extends StatefulWidget {
  final String title;

  // constructor
  VehiclesPage({Key key, this.title}) : super(key: key);

  @override
  _VehiclesPageState createState() => _VehiclesPageState();
}

class _VehiclesPageState extends State<VehiclesPage> {
  Widget _getIcon(type) {
    switch (type) {
      case VehicleType.car:
        return const Icon(Icons.directions_car);
      case VehicleType.moto:
      case VehicleType.bicyle:
        return const Icon(Icons.directions_bike);
      default:
        return const Icon(Icons.directions_car);
    }
  }

  Function deleteVehicleCallback(vehicleUid) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Center(child: CircularProgressIndicator());
        });
    UserService().delUserVehicle(vehicleUid).then((value) {
      setState(() {
        Navigator.pop(context);
      });
      Navigator.of(context, rootNavigator: true).pop(value);
    });
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Meus Veículos'),
      ),
      body: SingleChildScrollView(
        child: FutureBuilder<List<UserVehicle>>(
          future: UserService().getUserVehicles(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.hasData) {
              List<UserVehicle> vehicles = snapshot.data;

              if (vehicles.length > 0) {
                return Column(
                    children: vehicles.map<Widget>((vehicle) {
                  return Card(
                      color: Colors.white,
                      elevation: 3,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          ListTile(
                            leading: Icon(
                              Icons.directions_car,
                              size: 60,
                            ),
                            subtitle: Text(
                              vehicle.plate.toUpperCase(),
                              style: TextStyle(fontSize: 15),
                            ),
                            title: Text(
                              vehicle.type.name,
                              style: TextStyle(fontSize: 25),
                            ),
                          ),
                          ButtonBar(
                            children: <Widget>[
                              FlatButton(
                                child: Text(
                                  'Editar',
                                  style: TextStyle(
                                      color: Theme.of(context).hintColor),
                                ),
                                onPressed: () {/* ... */},
                              ),
                              FlatButton(
                                child: Text(
                                  'Apagar',
                                  style: TextStyle(
                                      color: Theme.of(context).hintColor),
                                ),
                                onPressed: () {
                                  Go4Dialog.questionAlert(
                                    'Esta ação irá remover o veículo selecionado, deseja continuar?',
                                    'Sim',
                                    'Cancelar',
                                    context,
                                    onPressed: () {
                                      deleteVehicleCallback(vehicle.uid);
                                    },
                                  );
                                },
                              ),
                            ],
                          ),
                        ],
                      ));
                }).toList());
              } else {
                return Container(
                    margin: const EdgeInsets.all(20.0),
                    padding: const EdgeInsets.all(10.0),
                    decoration: BoxDecoration(
                        border: Border.all(
                            style: BorderStyle.solid,
                            width: 1,
                            color: Color.fromRGBO(150, 150, 150,
                                0.7))), //       <--- BoxDecoration here
                    child: Center(
                      child: Text(
                        'Nenhum veículo cadastrado',
                        style: TextStyle(
                            fontSize: 20,
                            color: Color.fromRGBO(150, 150, 150, 0.7)),
                      ),
                    ));
              }
            } else {
              return ConstrainedBox(
                  constraints: BoxConstraints(
                      minHeight: MediaQuery.of(context).size.height - 100),
                  child: Center(
                      child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[CircularProgressIndicator()],
                  )));
            }
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(context,
                  MaterialPageRoute(builder: (context) => NewVehiclePage()))
              .then((value) {
            print(value);
            setState(() {});
          });
        },
        child: Icon(Icons.add),
      ),
    );
  }
}
