import 'dart:io';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_native_image/flutter_native_image.dart';
import 'package:go4electric/model/User.dart';
import 'package:go4electric/pages/floating_menu.dart';
import 'package:go4electric/pages/splash.dart';
import 'package:go4electric/pages/userData.dart';
import 'package:go4electric/service/auth.service.dart';
import 'package:image_picker/image_picker.dart';

class PerfilPage extends StatefulWidget {
  final String title;

  // constructor
  PerfilPage({Key key, this.title}) : super(key: key);

  @override
  _PerfilPageState createState() => _PerfilPageState();
}

class _PerfilPageState extends State<PerfilPage> {
  TextEditingController _displayNameController = TextEditingController();

  AuthService _auth = AuthService();
  User currentUser;

  void _logout() {
    _auth.signOut().then((result) {
      Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (BuildContext context) => Splash()),
        ModalRoute.withName('/'),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    File _image;

    Future<User> _currentUser = _auth.go4user.then((user) async {
      await user.getUserData();
      print(user);
      _displayNameController.text = user.displayName;
      currentUser = user;
      currentUser.photo = user.photo != null ? user.photo : "users/no-avatar.jpg";
      return user;
    });

    Future<String> _uploadFile() async {
      if (_image != null) {
        return "users/${currentUser.phone}/avatar.png";
      }
      return null;
    }

    Future<void> _saveImage(File image) async {
      if (image != null) {
        final StorageReference ref = FirebaseStorage()
            .ref()
            .child('users')
            .child('${currentUser.phone}')
            .child('avatar.png');
        final StorageUploadTask uploadTask = ref.putFile(
          image,
          StorageMetadata(
            contentType: 'image/png',
            customMetadata: <String, String>{
              'userImage': currentUser.displayName
            },
          ),
        );

        StorageTaskSnapshot storageTaskSnapshot = await uploadTask.onComplete;
        await storageTaskSnapshot.ref.getDownloadURL();

        await currentUser.save(photo: 'users/${currentUser.phone}/avatar.png');
      }
    }

    Future getImageCamera() async {
      var image = await ImagePicker.pickImage(source: ImageSource.camera);

      File compressedFile = await FlutterNativeImage.compressImage(image.path,
          quality: 40, percentage: 40);

      _saveImage(compressedFile).then((x) {
        setState(() {});
      });
    }

    Future getImageGallery() async {
      var image = await ImagePicker.pickImage(source: ImageSource.gallery);

      File compressedFile = await FlutterNativeImage.compressImage(image.path,
          quality: 40, percentage: 40);

      setState(() {});
      _saveImage(compressedFile).then((x) {
        setState(() {});
      });
    }

    Future<Widget> _getImage(BuildContext context, String image) async {
      Image m;
      await FirebaseStorage()
          .ref()
          .child(image)
          .getDownloadURL()
          .then((downladURL) {
        if (downladURL == "") {
          downladURL = "users/no-avatar.jpg";
        }
        m = Image.network(
          downladURL.toString(),
          fit: BoxFit.fitWidth,
        );
      });
      return m;
    }

    Widget _userPicture() {
      final _name = currentUser.displayName;
      final _phone = currentUser.phone;
      return Padding(
          padding: const EdgeInsets.all(10),
          child: Row(
            children: <Widget>[
              Container(
                width: 150,
                height: 150,
                margin: const EdgeInsets.symmetric(vertical: 8),
                child: Stack(
                  children: <Widget>[
                    ClipRRect(
                      borderRadius: BorderRadius.circular(80),
                      child: FutureBuilder(
                        future: _getImage(context, currentUser.photo),
                        builder: (context, snapshot) {
                          if (snapshot.connectionState ==
                              ConnectionState.done) {
                            return Container(
                                width: 140, height: 140, child: snapshot.data);
                          }
                          if (snapshot.connectionState ==
                              ConnectionState.waiting)
                            return Container(
                                height: 140,
                                width: 140,
                                child: Center(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: <Widget>[
                                      CircularProgressIndicator()
                                    ],
                                  ),
                                ));
                        },
                      ),
                    ),
                    Positioned(
                        top: -28,
                        left: 100,
                        child: FloatingMenu(
                          tooltag: "Go4E",
                          button3: ButtonInfo(
                              buttonIcon: Icons.camera_alt,
                              callFunc: getImageCamera,
                              toolTip: "Camera"),
                          button2: ButtonInfo(
                              buttonIcon: Icons.photo_library,
                              callFunc: getImageGallery,
                              toolTip: "Galeria"),
                        )),
                  ],
                ),
              ),
              Expanded(
                child: Column(
                  children: <Widget>[
                    Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: Container(
                          child: Text(
                            'Olá, $_name',
                            textAlign: TextAlign.left,
                            overflow: TextOverflow.clip,
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 22),
                          ),
                        )),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Text(
                        '$_phone',
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            fontSize: 15, fontStyle: FontStyle.italic),
                      ),
                    )
                  ],
                ),
              )
            ],
          ));
    }

    Widget _myData() {
      return ListTile(
          leading: Icon(Icons.account_box),
          title: Text(
            'Meus dados pessoais',
            style: TextStyle(color: Theme.of(context).hintColor, fontSize: 16),
          ),
          onTap: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => UserDataPage()));
          },
          trailing: Icon(
            Icons.arrow_forward_ios,
          ));
    }

    Widget _logoutBtn() {
      return ListTile(
          leading: Icon(Icons.exit_to_app),
          title: Text(
            'Sair',
            style: TextStyle(color: Theme.of(context).hintColor, fontSize: 16),
          ),
          onTap: () {
            _logout();
          });
    }

    Widget _creditCards() {
      return ListTile(
          leading: Icon(Icons.credit_card),
          title: Text(
            'Meus cartões',
            style: TextStyle(color: Theme.of(context).hintColor, fontSize: 16),
          ),
          trailing: Icon(
            Icons.arrow_forward_ios,
          ));
    }

    return Scaffold(
        appBar: AppBar(
          title: Text('Perfil'),
        ),
        body: FutureBuilder(
          future: _currentUser,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.hasData) {
              return SingleChildScrollView(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      _userPicture(),
                      Divider(color: Theme.of(context).hintColor),
                      _myData(),
                      Divider(color: Theme.of(context).hintColor),
                      _creditCards(),
                      Divider(color: Theme.of(context).hintColor),
                      _logoutBtn(),
                    ]),
              );
            } else {
              return Center(
                  child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[CircularProgressIndicator()],
              ));
            }
          },
        ));
  }
}
