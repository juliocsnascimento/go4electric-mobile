import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:go4electric/helpers/common.dart';
import 'package:go4electric/pages/splash.dart';
import 'package:go4electric/service/auth.service.dart';

import '../helpers/common.dart';
import '../helpers/exceptions.dart';

class PhoneCodePage extends StatefulWidget {
  PhoneCodePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _PhoneCodePageState createState() => _PhoneCodePageState();
}

class _PhoneCodePageState extends State<PhoneCodePage> {
  AuthService auth = AuthService();
  TextEditingController _codeController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    void _checkSMSCode() async {
      if (auth.verificationId != "") {
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return Center(child: CircularProgressIndicator());
            });
        auth.checkPhoneCode(_codeController.text).then((result) async {
          await Future.delayed(Duration(seconds: 3));
          Navigator.of(context, rootNavigator: true).pop(result);

          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (BuildContext context) => Splash()),
            ModalRoute.withName('/'),
          );
        }).catchError((e) {
          if (e.code == ErrorCode.wrong_code) {
            Go4Dialog.showAlert(
                    "Código incorreto! \n por favor, tente novamente.", context)
                .then((x) {
              _codeController.text = "";
              Navigator.of(context, rootNavigator: true).pop(x);
              Navigator.pop(context);
            });
          }
        });
      } else {
        Fluttertoast.showToast(
          msg: "É necessário preencher o codigo",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 2,
          backgroundColor: Theme.of(context).errorColor,
          textColor: Colors.white,
        );
      }
    }

    Widget _go4Logo() {
      return Padding(
          padding: const EdgeInsets.only(top: 10.0),
          child: Image.asset("assets/logo.png", scale: 2, width: 380));
    }

    Widget _title() {
      return Padding(
        padding: const EdgeInsets.only(
            top: 25, bottom: 40.0, left: 30.0, right: 30.0),
        child: Text(
          "DIGITE O CÓDIGO QUE ACABAMOS DE ENVIAR POR SMS",
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.body2,
        ),
      );
    }

    Widget _code() {
      return Container(
        child: Padding(
          padding: const EdgeInsets.only(top: 30, left: 30.0, right: 30.0),
          child: TextField(
            keyboardType: TextInputType.numberWithOptions(),
            controller: _codeController,
            style: Theme.of(context).textTheme.body1,
            decoration: InputDecoration(
                labelText: "CÓDIGO",
                labelStyle: Theme.of(context).textTheme.caption),
          ),
        ),
      );
    }

    Widget _sendButton() {
      return Padding(
          padding: const EdgeInsets.only(top: 30.0, left: 30, right: 30),
          child: PrimaryButton(
              buttonText: "CONFIRMAR",
              width: double.infinity,
              onPressed: () async {
                _checkSMSCode();
              }));
    }

    Widget _resend() {
      return Padding(
        padding: const EdgeInsets.only(top: 30.0),
        child: FlatButton(
          child: Text(
            "Enviar novamente",
            style: TextStyle(
              color: Theme.of(context).primaryColor,
              fontSize: 18.0,
            ),
          ),
          onPressed: () async {
            Navigator.pop(context);
          },
        ),
      );
    }

    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);

        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: BasicLayout(
          child: SingleChildScrollView(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
            _go4Logo(),
            _title(),
            _code(),
            _sendButton(),
            _resend()
          ]))),
    );
  }
}
