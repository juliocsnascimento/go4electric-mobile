import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:go4electric/helpers/common.dart';
import 'package:go4electric/model/Vehicle.dart';
import 'package:go4electric/service/user.service.dart';
import 'package:go4electric/service/vehicleService.dart';

class NewVehiclePage extends StatefulWidget {
  final String title;

  // constructor
  NewVehiclePage({Key key, this.title}) : super(key: key);

  @override
  _NewVehiclePageState createState() => _NewVehiclePageState();
}

class _NewVehiclePageState extends State<NewVehiclePage> {
  TextEditingController _plateController =
      MaskedTextController(mask: 'AAA-0000');
  Vehicle _selectedVehicle = new Vehicle();

  void _submitForm() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Center(
            child: CircularProgressIndicator(),
          );
        });

    UserService()
        .saveUserVehicle(_plateController.text, _selectedVehicle.uid)
        .then((value) {
      Navigator.of(context, rootNavigator: true).pop(value);
      Navigator.pop(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);

        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: Scaffold(
          appBar: AppBar(
            title: Text('Novo Veículo'),
          ),
          body: Padding(
              padding: const EdgeInsets.only(top: 30),
              child: FutureBuilder<List<Vehicle>>(
                future: VehicleService().getVehicles(),
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                  if (snapshot.hasData) {
                    return Form(
                      child: SingleChildScrollView(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              padding: const EdgeInsets.only(
                                  bottom: 20.0, left: 30, right: 30),
                              child: TextFormField(
                                textInputAction: TextInputAction.done,
                                controller: _plateController,
                                decoration: InputDecoration(
                                    labelText: "Placa",
                                    labelStyle:
                                        Theme.of(context).textTheme.caption),
                              ),
                            ),
                            Container(
                                padding: const EdgeInsets.only(
                                    bottom: 20.0, left: 30, right: 30),
                                child: DropdownButton(
                                  isExpanded: true,
                                  value: _selectedVehicle.uid,
                                  hint: Text('Modelo'),
                                  items: snapshot.data
                                      .map<DropdownMenuItem<String>>((vehicle) {
                                    return DropdownMenuItem<String>(
                                      child: Text(vehicle.name),
                                      value: vehicle.uid,
                                    );
                                  }).toList(),
                                  onChanged: (newVal) {
                                    setState(() => _selectedVehicle =
                                        new Vehicle(uid: newVal));
                                  },
                                )),
                            Container(
                              padding: const EdgeInsets.only(
                                  top: 10.0, left: 30, right: 30),
                              child: PrimaryButton(
                                width: double.infinity,
                                buttonText: "Salvar",
                                onPressed: () {
                                  _submitForm();
                                },
                              ),
                            )
                          ],
                        ),
                      ),
                    );
                  } else {
                    return Center(
                        child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[CircularProgressIndicator()],
                    ));
                  }
                },
              ))),
    );
  }
}
