import 'package:country_code_picker/country_code.dart';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:go4electric/helpers/common.dart';
import 'package:go4electric/pages/phoneCodePage.dart';
import 'package:go4electric/service/auth.service.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginPage> {
  AuthService auth = AuthService();
  Future<SharedPreferences> _sprefs = SharedPreferences.getInstance();
  TextEditingController _nameController = TextEditingController();
  TextEditingController _cellController =
      MaskedTextController(mask: '(00) 00000 - 0000');
  String countryCode;
  String countryName;

  Future<Null> getData() async {
    final SharedPreferences prefs = await _sprefs;
    String data1 = (prefs.getString('nameLogin'));
    String data2 = (prefs.getString('cellLogin'));
    if (data2 != null && data2[0] == "+") {
      countryCode = data2.substring(0, 3);
      data2 = data2.substring(3);

      this.setState(() {
        _nameController.text = data1;
        print(data1);
        _cellController.text = data2;
        print(data2);
      });
    }
  }

  Future<Null> saveData() async {
    final SharedPreferences prefs = await _sprefs;
    final intRegex = RegExp(r'\D', multiLine: true);
    prefs.setString('nameLogin', _nameController.text);
    prefs.setString('cellLogin',
        countryCode + _cellController.text.replaceAll(intRegex, ''));
  }

  @override
  void initState() {
    this.countryCode = '+55';
    this.countryName = 'BR';
    super.initState();
    getData();
  }

  @override
  Widget build(BuildContext context) {
    final focus = FocusNode();

    void _submitForm() {
      if (_cellController.text != "" && _nameController.text.trim() != "") {
        final intRegex = RegExp(r'\D', multiLine: true);

        final finalCellNumber =
            countryCode + _cellController.text.replaceAll(intRegex, '');

        auth
            .signUpByPhone(finalCellNumber, _nameController.text, context)
            .then((usuario) {
          saveData();
          Navigator.push(
              context,
              new MaterialPageRoute(
                  maintainState: false,
                  fullscreenDialog: true,
                  builder: (BuildContext context) => new PhoneCodePage(
                        title: "Confirm Login",
                      )));
        });
      } else {
        Fluttertoast.showToast(
          msg: "É necessário preencher todos os campos",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.TOP,
          timeInSecForIos: 2,
          backgroundColor: Theme.of(context).errorColor,
          textColor: Colors.white,
        );
      }
    }

    Widget _go4Logo() {
      return Padding(
          padding: const EdgeInsets.only(top: 10.0),
          child:
              Image.asset("assets/logo.png", scale: 2, width: double.infinity));
    }

    Widget _inputName() {
      return Padding(
        padding: const EdgeInsets.only(bottom: 20.0, left: 30, right: 30),
        child: Container(
          child: TextFormField(
            textCapitalization: TextCapitalization.words,
            textInputAction: TextInputAction.next,
            controller: _nameController,
            decoration: InputDecoration(
              labelText: "Nome",
            ),
            onFieldSubmitted: (v) {
              FocusScope.of(context).requestFocus(focus);
            },
          ),
        ),
      );
    }

    Widget _inputCellphone() {
      return Padding(
          padding: const EdgeInsets.only(bottom: 50.0, left: 30, right: 30),
          child: Container(
              child: TextFormField(
            controller: _cellController,
            keyboardType: TextInputType.phone,
            textInputAction: TextInputAction.done,
            decoration: InputDecoration(
              prefix: CountryCodePicker(
                onChanged: _onCountryChange,
                initialSelection: countryCode ?? 'BR',
              ),
              labelText: "Celular",
            ),
            onFieldSubmitted: (v) {
              _submitForm();
            },
          )));
    }

    Widget _submitButton() {
      return Padding(
        padding: const EdgeInsets.only(top: 30.0, left: 30, right: 30),
        child: PrimaryButton(
          width: double.infinity,
          buttonText: "ENTRAR",
          onPressed: () {
            _submitForm();
          },
        ),
      );
    }

    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);

        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: BasicLayout(
          child: Form(
              child: SingleChildScrollView(
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
            _go4Logo(),
            _inputName(),
            _inputCellphone(),
            _submitButton()
          ])))),
    );
  }

  void _onCountryChange(CountryCode countryCode) {
    this.countryCode = countryCode.dialCode;
    this.countryName = countryCode.code;
  }
}
