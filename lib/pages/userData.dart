import 'package:cpfcnpj/cpfcnpj.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:go4electric/helpers/common.dart';
import 'package:go4electric/model/User.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:go4electric/service/auth.service.dart';
import 'package:intl/intl.dart';

class UserDataPage extends StatefulWidget {
  final String title;

  // constructor
  UserDataPage({Key key, this.title}) : super(key: key);

  @override
  _UserDataPageState createState() => _UserDataPageState();
}

class _UserDataPageState extends State<UserDataPage> {
  final _focusNode = FocusNode();
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
  final dateFormat = DateFormat("yyyy-MM-dd");
  DateTime date;
  TextEditingController _nameController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _birthdayController = TextEditingController();
  TextEditingController _cpfController =
      MaskedTextController(mask: '000.000.000-00');

  AuthService _auth = AuthService();
  User currentUser;

  @override
  void initState() {
    super.initState();
    _focusNode.addListener(() {
      if (CPF.isValid(_cpfController.text)) {
        print("Este CPF é válido.");
      } else {
        print("Este CPF é inválido.");
      }
    });
  }

  @override
  void dispose() {
    _focusNode.dispose();
    super.dispose();
  }

  void _submitForm() {
    if (_fbKey.currentState.validate()) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return Center(
              child: CircularProgressIndicator(),
            );
          });
      currentUser.name = _nameController.text;
      currentUser.cpf = _cpfController.text;
      currentUser.email = _emailController.text;
      currentUser.birthDay = DateTime.parse(_birthdayController.text);

      try {
        currentUser.save().then((value) {
          Navigator.of(context, rootNavigator: true).pop(value);
          Navigator.pop(context);
        });
      } catch (e) {
        print(e);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    Future<User> _currentUser = _auth.go4user.then((user) async {
      await user.getUserData();
      print(user);
      _nameController.text = user.name;
      _emailController.text = user.email;
      _cpfController.text = user.cpf;
      _birthdayController.text =
          user.birthDay != null ? dateFormat.format(user.birthDay.toUtc()) : '';
      currentUser = user;
      return user;
    });

    Widget _submitButton() {
      return Padding(
        padding: const EdgeInsets.symmetric(vertical: 20.0),
        child: PrimaryButton(
          width: double.infinity,
          buttonText: "Salvar",
          onPressed: () {
            if (_fbKey.currentState.saveAndValidate()) {}
          },
        ),
      );
    }

    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);

        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: Scaffold(
          appBar: AppBar(
            title: Text('Dados Pessoais'),
          ),
          body: Padding(
            padding: const EdgeInsets.only(top: 30),
            child: FutureBuilder(
              future: _currentUser,
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                if (snapshot.hasData) {
                  return FormBuilder(
                    key: _fbKey,
                    initialValue: {
                      'email': currentUser.email,
                      'name': (currentUser.name != "" || currentUser.name == null)
                          ? currentUser.name
                          : currentUser.displayName,
                      'cpf': currentUser.cpf,
                      'birthday': currentUser.birthDay
                    },
                    child: Container(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            FormBuilderTextField(
                              attribute: 'name',
                              validators: [
                                FormBuilderValidators.required(
                                    errorText: "Nome é obrigatório"),
                              ],
                              textCapitalization: TextCapitalization.words,
                              textInputAction: TextInputAction.next,
                              decoration: InputDecoration(
                                labelText: "Nome Completo",
                              ),
                            ),
                            FormBuilderTextField(
                              attribute: 'email',
                              validators: [
                                FormBuilderValidators.required(
                                    errorText: "E-mail é obrigatório"),
                                FormBuilderValidators.email(
                                    errorText: "E-mail inválido"),
                              ],
                              textCapitalization: TextCapitalization.none,
                              textInputAction: TextInputAction.next,
                              decoration: InputDecoration(
                                labelText: "E-mail",
                              ),
                            ),
                            FormBuilderTextField(
                              attribute: 'cpf',
                              validators: [
                                FormBuilderValidators.required(
                                    errorText: "CPF é obrigatório"),
                                (val) {
                                  if (!CPF.isValid(val)) {
                                    return "CPF inválido";
                                  }
                                  return null;
                                },
                              ],
                              keyboardType: TextInputType.number,
                              textInputAction: TextInputAction.next,
                              decoration: InputDecoration(
                                labelText: "CPF",
                              ),
                            ),
                            FormBuilderDateTimePicker(
                              attribute: "birthday",
                              inputType: InputType.date,
                              format: DateFormat("yyyy-MM-dd"),
                              decoration: InputDecoration(
                                  labelText: "Data de Nascimento"),
                            ),
                            _submitButton(),
                          ]),
                    ),
                  );
                } else {
                  return Center(
                      child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[CircularProgressIndicator()],
                  ));
                }
              },
            ),
          )),
    );
  }
}
