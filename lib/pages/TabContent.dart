import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:flutter/material.dart';
import 'package:go4electric/pages/aboutPage.dart';
import 'package:go4electric/pages/historyPage.dart';
import 'package:go4electric/pages/homePage.dart';
import 'package:go4electric/pages/perfilPage.dart';
import 'package:go4electric/pages/plugshare.dart';

class TabContent extends StatefulWidget {
  final String title;

  // constructor
  TabContent({Key key, this.title}) : super(key: key);

  @override
  _TabContentState createState() => _TabContentState();
}

class _TabContentState extends State<TabContent> {
  //State class
  int _currentIndex = 0; // only Perfil for while;
  PageController _pageController;

  @override
  void initState() {
    super.initState();
    _pageController = PageController();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SizedBox.expand(
          child: PageView(
            controller: _pageController,
            onPageChanged: (index) {
              setState(() => _currentIndex = index);
            },
            children: <Widget>[
              HomePage(),
              HistoryPage(),
              PlugSharePage(),
              PerfilPage(),
              AboutPage(title: 'Sobre nós',),
            ],
          ),
        ),
        bottomNavigationBar: BottomNavyBar(
            selectedIndex: _currentIndex,
            itemCornerRadius: 20,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            curve: Curves.decelerate,
            onItemSelected: (index) {
              setState(() => _currentIndex = index);
              _pageController.jumpToPage(_currentIndex);
            },
            items: [
              BottomNavyBarItem(
                  textAlign: TextAlign.center,
                  icon: Icon(Icons.home),
                  title: Text('Home'),
                  activeColor: Theme.of(context).primaryColor),
              BottomNavyBarItem(
                  textAlign: TextAlign.center,
                  icon: Icon(Icons.monetization_on),
                  title: Text('Histórico'),
                  activeColor: Theme.of(context).primaryColor),
              BottomNavyBarItem(
                  textAlign: TextAlign.center,
                  icon: Icon(Icons.map),
                  title: Text('PlugShare'),
                  activeColor: Theme.of(context).primaryColor),
              BottomNavyBarItem(
                  textAlign: TextAlign.center,
                  icon: Icon(Icons.person),
                  title: Text('Perfil'),
                  activeColor: Theme.of(context).primaryColor),
              BottomNavyBarItem(
                  icon: Icon(Icons.info),
                  textAlign: TextAlign.center,
                  title: Text('Sobre'),
                  activeColor: Theme.of(context).primaryColor)
            ]));
  }
}
