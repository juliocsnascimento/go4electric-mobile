import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:go4electric/service/appConfig.service.dart';

class TermsOfUse extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Terms de Uso'),
        // iconTheme: IconThemeData(
        //   color: Colors.white,
        // ),
      ),
      body: FutureBuilder(
          future: AppConfig().getTermsOfUse(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.hasData) {
              return Padding(
                padding: const EdgeInsets.all(15),
                child: SingleChildScrollView(child: Html(data: snapshot.data)),
              );
            } else {
              return Center(
                  child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[CircularProgressIndicator()],
              ));
            }
          }),
    );
  }
}
