import 'package:flutter/material.dart';

typedef Future ExecFunc();

class ButtonInfo {
  final IconData buttonIcon;
  final ExecFunc callFunc;
  final String toolTip;
  ButtonInfo({this.buttonIcon, this.callFunc, this.toolTip});
}

class FloatingMenu extends StatefulWidget {
  //final ButtonInfo button1;
  final ButtonInfo button2;
  final ButtonInfo button3;
  final String tooltag;

  FloatingMenu({this.tooltag, this.button2, this.button3});

  @override
  _FloatingMenuState createState() => _FloatingMenuState(
      tooltag: tooltag, button2: button2, button3: button3);
}

class _FloatingMenuState extends State<FloatingMenu>
    with SingleTickerProviderStateMixin {
  bool isOpened = false;
  AnimationController _animationController;
  Animation<Color> _buttonColor;
  Animation<double> _animateIcon;
  Animation<double> _translateButton;
  Curve _curve = Curves.easeOut;
  double _fabHeight = 48.0;
  final String tooltag;
  //final ButtonInfo button1;
  final ButtonInfo button2;
  final ButtonInfo button3;

  _FloatingMenuState({this.tooltag, this.button2, this.button3});

  @override
  initState() {
    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 500))
          ..addListener(() {
            setState(() {});
          });
    _animateIcon =
        Tween<double>(begin: 0.0, end: 1.0).animate(_animationController);
    _buttonColor = ColorTween(
      begin: Colors.black,
      end: Colors.grey,
    ).animate(CurvedAnimation(
      parent: _animationController,
      curve: Interval(
        0.00,
        1.00,
        curve: Curves.linear,
      ),
    ));
    _translateButton = Tween<double>(
      begin: _fabHeight,
      end: 0.0,
    ).animate(CurvedAnimation(
      parent: _animationController,
      curve: Interval(
        0.0,
        0.75,
        curve: _curve,
      ),
    ));
    super.initState();
  }

  @override
  dispose() {
    _animationController.dispose();
    super.dispose();
  }

  animate() {
    if (!isOpened) {
      _animationController.forward();
    } else {
      _animationController.reverse();
    }
    isOpened = !isOpened;
  }
/*
  Widget option1() {
    return Container(
      child: FloatingActionButton(
        heroTag: "teste1" + tooltag,
        onPressed: button1.callFunc,
        tooltip: button1.toolTip,
        mini: true,
        child: Icon(button1.buttonIcon),
      ),
    );
  }
*/
  Widget option2() {
    return Container(
      child: FloatingActionButton(
        heroTag: "teste2" + tooltag,
        onPressed: button2.callFunc,
        tooltip: button2.toolTip,
        mini: true,
        child: Icon(button2.buttonIcon),
      ),
    );
  }

  Widget option3() {
    return Container(
      child: FloatingActionButton(
        heroTag: "teste3" + tooltag,
        onPressed: button3.callFunc,
        tooltip: button3.toolTip,
        mini: true,
        child: Icon(button3.buttonIcon),
      ),
    );
  }

  Widget toggle() {
    return Container(
      child: FloatingActionButton(
        heroTag: "teste4" + tooltag,
        backgroundColor: _buttonColor.value,
        onPressed: animate,
        tooltip: 'Toggle',
        elevation: 0,
        highlightElevation: 0,
        mini: true,
        child: AnimatedIcon(
          color: Colors.white,
          icon: AnimatedIcons.menu_close,
          progress: _animateIcon,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 200.0,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            /*Transform(
              transform: Matrix4.translationValues(
                0.0,
                _translateButton.value * 3.0,
                0.0,
              ),
              child: option1(),
            ),*/
            Transform(
              transform: Matrix4.translationValues(
                0.0,
                _translateButton.value * 2.0,
                0.0,
              ),
              child: option2(),
            ),
            Transform(
              transform: Matrix4.translationValues(
                0.0,
                _translateButton.value,
                0.0,
              ),
              child: option3(),
            ),
            toggle(),
          ],
        ));
  }
}
