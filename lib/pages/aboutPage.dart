import 'package:flutter/material.dart';
import 'package:go4electric/pages/privacyPolicy.dart';
import 'package:go4electric/pages/termsOfUse.dart';
// import 'package:go4electric/helpers/common.dart';

class AboutPage extends StatefulWidget {
  final String title;

  // constructor
  AboutPage({Key key, this.title}) : super(key: key);

  @override
  _AboutPageState createState() => _AboutPageState();
}

class _AboutPageState extends State<AboutPage> {
  Widget _privacyPolice() {
    return ListTile(
        title: Text(
          'Política de Privacide',
          style: TextStyle(color: Theme.of(context).hintColor, fontSize: 15),
        ),
        onTap: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => PrivacyPolicy()));
        },
        trailing: Icon(
          Icons.arrow_forward_ios,
        ));
  }

  Widget _go4Lgo() {
    return Center(
      child: Image.asset("assets/logo.png", scale: 2, width: double.infinity),
    );
  }

  Widget _contacts() {
    return Padding(
        padding: const EdgeInsets.symmetric(
          vertical: 30,
          horizontal: 30,
        ),
        child: Column(
          children: <Widget>[
            ListTile(
              title: Text('+55 19 99999-9999',
                  style: TextStyle(
                      color: Theme.of(context).hintColor, fontSize: 16)),
              leading: Icon(Icons.phone_iphone),
            ),
            ListTile(
              title: Text('contato@go4electric.com.br',
                  style: TextStyle(
                      color: Theme.of(context).hintColor, fontSize: 16)),
              leading: Icon(Icons.mail_outline),
            )
          ],
        ));
  }

  Widget _termsOfUse() {
    return ListTile(
        title: Text(
          'Termos de Uso',
          style: TextStyle(color: Theme.of(context).hintColor, fontSize: 15),
        ),
        onTap: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => TermsOfUse()));
        },
        trailing: Icon(
          Icons.arrow_forward_ios,
        ));
  }

  Widget _aboutStore() {
    return ListTile(
        title: Text(
          'Sobre a Go4 na App Store',
          style: TextStyle(color: Theme.of(context).hintColor, fontSize: 15),
        ),
        trailing: Icon(
          Icons.arrow_forward_ios,
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Sobre nós'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            _go4Lgo(),
            _contacts(),
            Divider(color: Theme.of(context).hintColor),
            _aboutStore(),
            Divider(color: Theme.of(context).hintColor),
            _privacyPolice(),
            Divider(color: Theme.of(context).hintColor),
            _termsOfUse()
          ],
        ),
      ),
    );
  }
}
