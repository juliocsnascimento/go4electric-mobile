import 'dart:async';
import 'package:flutter/material.dart';
import 'package:location/location.dart';
import 'package:webview_flutter/webview_flutter.dart';

class PlugSharePage extends StatefulWidget {
  final String title;

  // constructor
  PlugSharePage({Key key, this.title}) : super(key: key);

  @override
  _PlugSharePageState createState() => _PlugSharePageState();
}

class _PlugSharePageState extends State<PlugSharePage> {
  final Completer<WebViewController> _controller =
      Completer<WebViewController>();
  var location = new Location();
  String lat, long;

  Widget _map(LocationData currentLocation) {
    return WebView(
        initialUrl:
            'https://www.plugshare.com/widget2.html?latitude=${currentLocation.latitude}&longitude=${currentLocation.longitude}&spanLat=0.07228&spanLng=0.07228&plugs=1,2,3,4,5,6,42,13,7,8,9,10,11,12,14,15,16,17',
        javascriptMode: JavascriptMode.unrestricted,
        gestureNavigationEnabled: true,
        onWebViewCreated: (WebViewController webViewController) {
          _controller.complete(webViewController);
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('Mapa PlugShare'),),
        body: FutureBuilder(
          future: location.getLocation(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.hasData) {
              if (snapshot.data != null) {
                return Scaffold(
                  body: _map(snapshot.data),
                );
              } else {
                return new CircularProgressIndicator();
              }
            } else {
              return new CircularProgressIndicator();
            }
          },
        ));
  }
}
