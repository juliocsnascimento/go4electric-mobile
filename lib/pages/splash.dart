import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:go4electric/pages/TabContent.dart';
import 'package:go4electric/pages/loginPage.dart';
// import 'package:go4eletric/service/auth.service.dart';
import 'package:shared_preferences/shared_preferences.dart';

Map<int, Color> go4Color = {
  50: Color.fromRGBO(238, 249, 248, 1),
  100: Color.fromRGBO(204, 238, 236, 1),
  200: Color.fromRGBO(171, 227, 224, 1),
  300: Color.fromRGBO(120, 210, 206, 1),
  400: Color.fromRGBO(87, 199, 194, 1),
  500: Color.fromRGBO(78, 179, 174, 1),
  600: Color.fromRGBO(60, 139, 135, 1),
  700: Color.fromRGBO(43, 99, 97, 1),
  800: Color.fromRGBO(34, 79, 77, 1),
  900: Color.fromRGBO(17, 39, 38, 1),
};

class Splash extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
        title: 'Go4Eletric',
        theme: ThemeData(
          brightness: Brightness.light,
            canvasColor: Colors.white,
            errorColor: Color(0xFFB00020),
            cardColor: Color.fromRGBO(199, 87, 92, 1),
            hintColor: Color.fromRGBO(150, 150, 150, 1),
            primaryColor: go4Color[400],
            buttonTheme: ButtonThemeData(textTheme: ButtonTextTheme.accent),
            primarySwatch: MaterialColor(0X000000, go4Color)),
        home: Builder(
          builder: (context) => new _SplashContent(),
        ),
        routes: <String, WidgetBuilder>{
          '/home': (BuildContext context) => new TabContent(),
          '/login': (BuildContext context) => new LoginPage(),
        });
  }
}

class _SplashContent extends StatefulWidget {
  @override
  _SplashContentState createState() => new _SplashContentState();
}

class _SplashContentState extends State<_SplashContent>
    with SingleTickerProviderStateMixin {
  final FirebaseAuth _authFire = FirebaseAuth.instance;
  // AuthService _authService;
  FirebaseUser _user;

  startTimeout() async {
    // _authService = AuthService();
    _user = await _authFire.currentUser();
    if (_user != null) {
      await _user.reload().catchError((e) {
        _authFire.signOut();
      });
      Future<SharedPreferences> _sprefs = SharedPreferences.getInstance();
      final SharedPreferences prefs = await _sprefs;
      await prefs.setString('user_uuid', _user.uid);
    }

    var _duration = new Duration(seconds: 5);
    return new Timer(_duration, navigationPage);
  }

  navigationPage() {
    if (_user != null) {
      Navigator.of(context).pushReplacementNamed('/home');
    } else {
      Navigator.of(context).pushReplacementNamed('/login');
    }
  }

  @override
  void initState() {
    super.initState();
    startTimeout();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        backgroundColor: Colors.teal,
        body: Container(
          child: Column(
            children: <Widget>[
              Container(
                padding: const EdgeInsets.only(
                    top: 100, left: 10, right: 10, bottom: 40),
                child: Center(
                  child: Image.asset("assets/logo-white.png",
                      scale: 2, width: double.infinity),
                ),
              ),
              Center(
                child: CircularProgressIndicator(
                  backgroundColor: Colors.white,
                ),
              )
            ],
          ),
          constraints: BoxConstraints.expand(),
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: [MaterialColor(0x62CCC7, go4Color), Colors.teal])),
        ));
  }
}
